﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DawidKobierskiLab5.Models
{
    public class Comment : ModelBase
    {
        public virtual Author Author { get; set; }
        [Required]
        [MinLength(5)]
        public string Content { get; set; }
    }
}