namespace WaldemarBednarczykZadLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Details_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Details_Id");
            AddForeignKey("dbo.Orders", "Details_Id", "dbo.OrderDetails", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Details_Id", "dbo.OrderDetails");
            DropIndex("dbo.Orders", new[] { "Details_Id" });
            DropColumn("dbo.Orders", "Details_Id");
        }
    }
}
