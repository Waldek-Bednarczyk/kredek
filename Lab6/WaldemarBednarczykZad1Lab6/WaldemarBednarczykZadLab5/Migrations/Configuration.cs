namespace WaldemarBednarczykZadLab5.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WaldemarBednarczykZadLab5.Models.FastFoodContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WaldemarBednarczykZadLab5.Models.FastFoodContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Orders.AddOrUpdate(
            p => p.Restaurant,
            new Models.Order { Restaurant = "KFC 123",Food = "B-smart", Drink = "Coca-Cola", PersonalData = new Models.OrderPersonalData {FirstName = "Anna", LastName="Nowak", Adress = "Nyska 20",PhoneNumber = "999333445"}},
            new Models.Order { Restaurant = "KFC 132", Food = "Tortilla", Drink="Pepsi", PersonalData = new Models.OrderPersonalData { FirstName = "Pawe�", LastName = "Nowak", Adress = "Nyska 20", PhoneNumber = "999333445" } },
            new Models.Order { Restaurant = "KFC 321", Food = "Cheesburger", Drink = "Fanta" , PersonalData = new Models.OrderPersonalData { FirstName = "Anna", LastName = "Nowak", Adress = "Nyska 20", PhoneNumber = "999333445" }}

            );
        }    
    }
}
