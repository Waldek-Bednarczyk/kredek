namespace WaldemarBednarczykZadLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "Restaurant", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Food", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Drink", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "Drink", c => c.String());
            AlterColumn("dbo.Orders", "Food", c => c.String());
            AlterColumn("dbo.Orders", "Restaurant", c => c.String());
        }
    }
}
