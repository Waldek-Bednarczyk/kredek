namespace WaldemarBednarczykZadLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Details_Id", "dbo.OrderDetails");
            DropIndex("dbo.Orders", new[] { "Details_Id" });
            DropColumn("dbo.Orders", "Details_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Details_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Details_Id");
            AddForeignKey("dbo.Orders", "Details_Id", "dbo.OrderDetails", "Id");
        }
    }
}
