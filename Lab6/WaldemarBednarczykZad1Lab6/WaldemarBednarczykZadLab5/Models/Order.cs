﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab5.Models
{
    public class Order : ModelBase
    {
        [Required]
        [MinLength(5)]
        public string Restaurant { get; set; }
        [Required]
        [MinLength(5)]
        public string Food { get; set; }
        [Required]
        [MinLength(5)]
        public string Drink { get; set; }
        public virtual OrderPersonalData PersonalData {get;set;}
        public virtual OrderDetails Details { get; set; }
    }
}