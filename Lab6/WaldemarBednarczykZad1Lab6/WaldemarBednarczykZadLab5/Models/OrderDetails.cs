﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab5.Models
{
    public class OrderDetails : ModelBase
    {
        public virtual OrderPersonalData PersonalData { get; set; }
        public string HowFar { get; set; }
        public string HowMuchTime { get; set; }
    }
}