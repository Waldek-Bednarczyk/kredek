﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab5.Models
{
    public class OrderPersonalData : ModelBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
    }
}