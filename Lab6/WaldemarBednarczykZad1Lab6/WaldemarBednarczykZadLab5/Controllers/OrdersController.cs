﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaldemarBednarczykZadLab5.Models;

namespace WaldemarBednarczykZadLab5.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Orders
        public ActionResult Index()
        {
            List<Order> orders;
            using (var ctx = new FastFoodContext())
            {
                orders = ctx.Orders.ToList();
            }
                return View(orders);
        }

        public ActionResult NewOrder()
        {
            return View(new Order());
        }

        [HttpPost]
        public ActionResult NewOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return View(order);
            }

            using (var ctx = new FastFoodContext())
            {
                ctx.Orders.Add(order);
                ctx.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Order order;

            using (var ctx = new FastFoodContext())
            {
                order = ctx.Orders.SingleOrDefault(p => p.Id == id);
            }
            return View(order);
        }

        [HttpPost]
        public ActionResult Edit(int id, Order order)
        {
            if (!ModelState.IsValid)
            {
                return View(order);
            }

            using (var ctx = new FastFoodContext())
            {
                var dbEntry = ctx.Orders.SingleOrDefault(p => p.Id == id);
                dbEntry.Restaurant = order.Restaurant;
                dbEntry.Food = order.Food;
                dbEntry.Drink = order.Drink;
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var ctx = new FastFoodContext())
            {
                var dbEntry = ctx.Orders.SingleOrDefault(p => p.Id == id);
                ctx.Orders.Remove(dbEntry);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}