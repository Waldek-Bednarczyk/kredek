namespace KrzysztofNowakowskiLab5.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KrzysztofNowakowskiLab5.Models.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(KrzysztofNowakowskiLab5.Models.BlogContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Posts.AddOrUpdate(
                p => p.Title,
                new Models.Post{Title = "Hello Kredek! 4", Body = "CPC-2017-1-Kredek", Author = new Models.Author {FirstName = "Krzysiek", LastName = "Nowakowski",  UserName = "knowakowski"} },
                new Models.Post{Title = "Hello Kredek! 5", Body = "CPC-2017-1-Kredek 2", Author = new Models.Author { FirstName = "Jerzy", LastName = "Gr�boszcz", UserName = "symfonia" } },
                new Models.Post{Title = "Hello Kredek! 6", Body = "CPC-2017-1-Kredek 3", Author = new Models.Author { FirstName = "Bjorne", LastName = "StaryStrup", UserName = "knowakowski" } }
                );
        }
    }
}
