// <auto-generated />
namespace KrzysztofNowakowskiLab5.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddValidationToPosts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddValidationToPosts));
        
        string IMigrationMetadata.Id
        {
            get { return "201705172038539_AddValidationToPosts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
