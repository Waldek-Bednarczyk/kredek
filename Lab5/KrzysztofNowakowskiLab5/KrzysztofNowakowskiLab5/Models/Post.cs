﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KrzysztofNowakowskiLab5.Models
{
    public class Post : ModelBase
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public virtual Author Author { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
    }
}