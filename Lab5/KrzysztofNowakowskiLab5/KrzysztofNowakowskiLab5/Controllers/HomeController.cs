﻿using KrzysztofNowakowskiLab5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KrzysztofNowakowskiLab5.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Post> posts;
            using(var ctx = new BlogContext())
            {
                posts = ctx.Posts.ToList();
            }
            return View(posts);
        }
    }
}