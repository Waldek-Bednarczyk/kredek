﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab5.Models
{
    public class Order : ModelBase
    {
        public string Restaurant { get; set; }
        public string Food { get; set; }
        public string Drink { get; set; }
        public virtual OrderPersonalData PersonalData {get;set;}
        public virtual IEnumerable <Order> Orders { get; set; }
    }
}