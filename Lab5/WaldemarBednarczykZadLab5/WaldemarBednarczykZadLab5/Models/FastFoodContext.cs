﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab5.Models
{
    public class FastFoodContext : DbContext
    {
        public FastFoodContext() : base("DefaultConnection") { }

        public IDbSet <Order> Orders { get; set; }
        public IDbSet <OrderDetails> Details { get; set; }

    }
}