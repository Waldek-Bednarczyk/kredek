﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaldemarBednarczykZadLab5.Models;

namespace WaldemarBednarczykZadLab5.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Order> orders;
            using (var ctx = new FastFoodContext())
            {
                orders = ctx.Orders.ToList();
            }
                return View(orders);
        }
    }
}