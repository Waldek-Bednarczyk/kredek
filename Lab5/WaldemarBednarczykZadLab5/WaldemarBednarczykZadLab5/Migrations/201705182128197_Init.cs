namespace WaldemarBednarczykZadLab5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HowFar = c.String(),
                        HowMuchTime = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        PersonalData_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderPersonalDatas", t => t.PersonalData_Id)
                .Index(t => t.PersonalData_Id);
            
            CreateTable(
                "dbo.OrderPersonalDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Adress = c.String(),
                        PhoneNumber = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Restaurant = c.String(),
                        Food = c.String(),
                        Drink = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        PersonalData_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderPersonalDatas", t => t.PersonalData_Id)
                .Index(t => t.PersonalData_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "PersonalData_Id", "dbo.OrderPersonalDatas");
            DropForeignKey("dbo.OrderDetails", "PersonalData_Id", "dbo.OrderPersonalDatas");
            DropIndex("dbo.Orders", new[] { "PersonalData_Id" });
            DropIndex("dbo.OrderDetails", new[] { "PersonalData_Id" });
            DropTable("dbo.Orders");
            DropTable("dbo.OrderPersonalDatas");
            DropTable("dbo.OrderDetails");
        }
    }
}
