﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WaldemarBednarczykZadLab5.Startup))]
namespace WaldemarBednarczykZadLab5
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
