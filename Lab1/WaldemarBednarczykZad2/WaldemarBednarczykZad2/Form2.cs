﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZad2
{
    public partial class FormDataEnter : Form
    {
        public FormDataEnter()
        {
            InitializeComponent();
            this.textBoxNumber.KeyPress += new KeyPressEventHandler(textBoxNumber_KeyPress);
            this.textBoxName.KeyPress += new KeyPressEventHandler(textBoxName_KeyPress);
            this.textBoxSurname.KeyPress += new KeyPressEventHandler(textBoxName_KeyPress);
        }
        //licznik czasu
        int count = 0;

        /// <summary>
        /// Rozpoczęcie pracy timera w Form1
        /// </summary>
        Timer measure = new Timer();
        public Timer Timer { get { return measure; } }

        /// <summary>
        /// Upływ czasu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerCount_Tick(object sender, EventArgs e)
        {
            count++;
            textBoxTime.Text = count.ToString();
            this.progressBarFill.Increment(1);
            //Gdy czas minął
            if (count == 120)
            {
                this.timerCount.Stop();
                MessageBox.Show("Przeznaczony czas minął" + "\n" +
                    "Spróbuj ponownie", "Koniec Czasu",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                this.Close();
            }
        }

        /// <summary>
        /// Zapisywanie danych do pliku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "" || textBoxNumber.Text == "" || 
                textBoxSurname.Text == "" || comboBoxGender.Text == "")
            {
                MessageBox.Show("Uzupełnij dane", "Uzupełnij",
                    MessageBoxButtons.OK,MessageBoxIcon.Stop);
            }
            else
            {
                //Sprawdzanie numeru pesel
                if (textBoxNumber.Text.Length != 11)
                {
                    MessageBox.Show("Numer Pesel jest niepoprawny",
                        "Błąd",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    textBoxNumber.BackColor = Color.Red;
                }
                else
                {
                    //Ścieżka do pulpitu
                    string createFolder = Environment.GetFolderPath(
                         System.Environment.SpecialFolder.DesktopDirectory);
                    //Zapis do pliku
                    using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(createFolder + @"\DaneOsobowe.txt", true))
                    {
                        
                        file.WriteLine(labelName.Text + ":" + textBoxName.Text);
                        file.WriteLine(labelSurname.Text + ":" + textBoxSurname.Text);
                        file.WriteLine(labelNumber.Text + ":" + textBoxNumber.Text);
                        file.WriteLine(labelDate.Text + ":" + dateTimePickerBirth.Text);
                        file.WriteLine(labelGender.Text + ":" + comboBoxGender.Text);
                        file.WriteLine("\n");
                    }
                    //zatrzymanie timera
                    this.timerCount.Stop();

                    MessageBox.Show("Dziękujemy za wprowadzenie danych" + "\n" +
                        "Dane zostały zapisane w pliku tekstowym na puplicie" + "\n" +
                        "Zostaniesz przeniesiony do ekranu początkowego" + "\n" + 
                        "Aby dodać kolejną osobę wybierz przycisk \"Rozpocznij\" ", 
                        "Dane poprawne",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                }
            }
        }

        /// <summary>
        /// Wpisywanie tylko cyfr oraz klawiszy np. Backspace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        /// <summary>
        /// Wpisywanie tylko tekstu oraz klawiszy np. Backspace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);

        }
        private void textBoxSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);

        }
        /// <summary>
        /// Przerwanie wprowadzania danych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Czy na pewno chces przerwać?",
                "Koniec",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes) this.Close();
        }
    }
}
