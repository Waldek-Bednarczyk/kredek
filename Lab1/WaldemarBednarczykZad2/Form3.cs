﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZad2
{
    public partial class FormReward : Form
    {
        public FormReward()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Rysowanie po wyborze co ma zostac narysowane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDraw_Click(object sender, EventArgs e)
        {
            if (this.comboBoxChoose.Text == "Koło") this.PaintCircle();
            if (this.comboBoxChoose.Text == "Gwiazda") this.PaintStar();
            if (this.comboBoxChoose.Text == "Prostokąt") this.PaintRectangle();
            if (this.comboBoxChoose.Text == "Kwadrat") this.PaintSquare();
            if (this.comboBoxChoose.Text == "Trójkąt") this.PaintTriangle();
            MessageBox.Show("Dziękujemy" + "\n" + "Do widzenia","Zakończono",MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            this.Close();
        }
        /// <summary>
        /// rysowanie kola
        /// </summary>
        public void PaintCircle()
        {
            System.Drawing.Graphics circle;
            circle = this.CreateGraphics();
            Pen myPen = new Pen(Color.Blue, 10);
            Rectangle myRectangle = new Rectangle(70,130,200,200) ;
            circle.DrawEllipse(myPen, myRectangle);
        }
        /// <summary>
        /// rysowanie gwiazdy
        /// </summary>
        public void PaintStar()
        {
            System.Drawing.Graphics star;
            star = this.CreateGraphics();
            Pen myPen = new Pen(Color.Blue, 10);
            Point point1 = new Point(150, 130);
            Point point2 = new Point(50, 302);
            Point point3 = new Point(250, 302);
            Point point4 = new Point(150, 360);
            Point point5 = new Point(50, 180);
            Point point6 = new Point(250, 180);
        
            Point[] howPointsUp = { point1, point2, point3 };
            Point[] howPointsDown = { point4, point5, point6 };
            star.DrawPolygon(myPen, howPointsUp);
            star.DrawPolygon(myPen, howPointsDown);

        }
        /// <summary>
        /// rysowanie prostokata
        /// </summary>
        public void PaintRectangle()
        {
            System.Drawing.Graphics rectangle;
            rectangle = this.CreateGraphics();
            Pen myPen = new Pen(Color.Blue, 10);
            rectangle.DrawRectangle(myPen, 60, 130, 220, 100);
        }
        /// <summary>
        /// rysowanie kwadratu
        /// </summary>
        public void PaintSquare()
        {
            System.Drawing.Graphics square;
            square = this.CreateGraphics();
            Pen myPen = new Pen(Color.Blue, 10);
            square.DrawRectangle(myPen,70, 130, 200, 200);
        }
        /// <summary>
        /// rysowanie trojkata
        /// </summary>
        public void PaintTriangle()
        {
            System.Drawing.Graphics triangle;
            triangle = this.CreateGraphics();
            Pen myPen = new Pen(Color.Blue, 10);
            Point point1 = new Point(150, 130);
            Point point2 = new Point(60, 360);
            Point point3 = new Point(240, 360);
            Point[] howPoints = { point1, point2, point3 };
            triangle.DrawPolygon(myPen, howPoints);
        }
    }
}
