﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZad2
{
    public partial class FormData : System.Windows.Forms.Form
    {
        public FormData()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Wyświetlenie formularza danych oraz start timera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEnter_Click(object sender, EventArgs e)
        {
            FormDataEnter formDataEnter = new FormDataEnter();
            formDataEnter.Show();
            formDataEnter.Timer.Start();
            MessageBox.Show("Na wypełnienie formularza przewidziano 2 minuty",
                "Informacja",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        /// <summary>
        /// Wyłączenie aplikacji
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEnd_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
