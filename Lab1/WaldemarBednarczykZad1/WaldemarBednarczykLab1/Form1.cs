﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykLab1
{
    public partial class FormLogin : Form
    {

        public FormLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Przycisk logowania oraz wyswietlania nowego Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if(textBoxLogin.Text == "test" && textBoxPassword.Text == "test")
            {
                MessageBox.Show("Dane logowania poprawne");
                textBoxLogin.BackColor = Color.Green;
                textBoxPassword.BackColor = Color.Green;
                FormLogged formLogged = new FormLogged();
                formLogged.Show();
                formLogged.Text = textBoxLogin.Text;
            }
            else
            {
                MessageBox.Show("Złe dane logowania");
                textBoxLogin.BackColor = Color.Red;
                textBoxPassword.BackColor = Color.Red;
            }
        }
    }
}
