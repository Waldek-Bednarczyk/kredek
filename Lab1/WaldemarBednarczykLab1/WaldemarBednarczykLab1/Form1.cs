﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykLab1
{
    public partial class FormLogin : Form
    {
        //licznik kliknięć
        public int counter = 0;

        //zmienna tekstowa
        string text = "przykladowy ekran";


        public FormLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Przycisk zamykający okno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if(textBoxLogin.Text == "test" && textBoxPassword.Text == "test")
            {
                MessageBox.Show("Dane logowania poprawne");
                textBoxLogin.BackColor = Color.Green;
                textBoxPassword.BackColor = Color.Green;
            }
            else
            {
                MessageBox.Show("Złe dane logowania");
                textBoxLogin.BackColor = Color.Red;
                textBoxPassword.BackColor = Color.Red;
            }
        }
        /// <summary>
        /// Przykładowa pętla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoop_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                textBoxLogin.Text += i + 1;
            }
        }
        /// <summary>
        /// Przycisk tworzący nowe okno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNewWindow_Click(object sender, EventArgs e)
        {
            FormLogged formLogged = new FormLogged();
            formLogged.Show();
            formLogged.Text = textBoxLogin.Text;
        }
    }
}
