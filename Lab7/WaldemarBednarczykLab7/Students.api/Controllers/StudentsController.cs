﻿using Students.Dbo;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace Students.api.Controllers
{
    [RoutePrefix("api/students")]
    public class StudentsController : StudentsBaseController
    {
        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<Student>))]
        public IHttpActionResult Get()
        {
            return Ok(Students.Values);
        }

        [HttpGet, Route("{id:int}", Name = "GetStudent")]
        [ResponseType(typeof(Student))]
        public IHttpActionResult Get(int id)
        {
            if(!Students.ContainsKey(id))
            {
                return NotFound();
            }
            return Ok(Students[id]);
        }

        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody] Student student)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var maxId = Students.Count > 0 ? Students.Keys.Max() : 0;
            student.Id = ++maxId;
            Students.Add(student.Id, student);

            return CreatedAtRoute("GetStudent", new { id = student.Id }, student);
        }
        [HttpDelete, Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            if(!Students.ContainsKey(id))
            {
                return NotFound();
            }
            Students.Remove(id);
            return Ok();
        }
        [HttpPut, Route("{id:int}")]
        public IHttpActionResult Put(int id, [FromBody] Student editedStudent)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!Students.ContainsKey(id))
            {
                return NotFound();
            }
            Student student = Students[id];

            student.FirstName = editedStudent.FirstName;
            student.LastName = editedStudent.LastName;
            student.HomeTown = editedStudent.HomeTown;
            student.SchoolIndex = editedStudent.SchoolIndex;

            return Ok();
        }
    }
}
