﻿using Students.Dbo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Students.api.Controllers
{
    public abstract class StudentsBaseController : ApiController
    {
        protected static readonly Dictionary <int, Student> Students;

        static StudentsBaseController()
        {
            Students = new Dictionary<int, Student>
            {
                [1] = new Student { Id = 1, FirstName = "Jan", LastName = "Kowalski", HomeTown = "Wrocław", SchoolIndex = "12345" },
                [2] = new Student { Id = 2, FirstName = "Patryk", LastName = "Nowak", HomeTown = "Warszawa", SchoolIndex = "67890" },
                [3] = new Student { Id = 3, FirstName = "Waldemar", LastName = "Bednarczyk", HomeTown = "Wrocław", SchoolIndex = "74245" }
            };
        }
    }
}
