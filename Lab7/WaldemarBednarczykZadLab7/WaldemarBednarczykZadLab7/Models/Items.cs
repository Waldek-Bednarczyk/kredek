﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaldemarBednarczykZadLab7.Models
{
    public class Items : ModelBase
    {
            public string Clothes { get; set; }
            public string Drinks { get; set; }
            public string Food { get; set; }
    }
}