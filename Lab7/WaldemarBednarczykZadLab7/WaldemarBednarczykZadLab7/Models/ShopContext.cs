﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace WaldemarBednarczykZadLab7.Models
{
    public class ShopContext : DbContext
    {
        public ShopContext() : base("DefaultConnection") { }

        public IDbSet<Shop> Shops { get; set; }
    }
}