﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WaldemarBednarczykZadLab7.Models;

namespace WaldemarBednarczykZadLab7.Controllers
{
    [RoutePrefix("api/shops")]
    public class ShopsMainController : ShopsController
    {
        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<Shop>))]
        public IHttpActionResult Get()
        {
            return Ok(Shops.Values);
        }

        [HttpGet, Route("{id:int}", Name = "GetShop")]
        [ResponseType(typeof(Shop))]
        public IHttpActionResult Get(int id)
        {
            if (!Shops.ContainsKey(id))
            {
                return NotFound();
            }
            return Ok(Shops[id]);
        }
        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody] Shop shop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var maxId = Shops.Count > 0 ? Shops.Keys.Max() : 0;
            shop.Id = ++maxId;
            Shops.Add(shop.Id, shop);

            return CreatedAtRoute("GetShop", new { id = shop.Id }, shop);
        }
        [HttpDelete, Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            if (!Shops.ContainsKey(id))
            {
                return NotFound();
            }
            Shops.Remove(id);
            return Ok();
        }
        [HttpPut, Route("{id:int}")]
        public IHttpActionResult Put(int id, [FromBody] Shop editedShops)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!Shops.ContainsKey(id))
            {
                return NotFound();
            }
            Shop shop = Shops[id];
            shop.Country = editedShops.Country;
            shop.City = editedShops.City;
            shop.Street = editedShops.Street;
            shop.StreetNumber = editedShops.StreetNumber;
            shop.Items.Clothes = editedShops.Items.Clothes;
            shop.Items.Drinks = editedShops.Items.Drinks;
            shop.Items.Food = editedShops.Items.Food;

            return Ok();
        }
    }
}
