﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using WaldemarBednarczykZadLab7.Models;

namespace WaldemarBednarczykZadLab7.Controllers
{
    public abstract class ShopsController : ApiController
    {
        protected static readonly Dictionary<int, Shop> Shops;
        static ShopsController()
        {
            Shops = new Dictionary<int, Shop>
            {
               [1] = new Shop {Id = 1, Country = "Polska", City = "Wrocław", Street = "Grunwaldzka", StreetNumber = "32", Items = new Items {Id = 1, Clothes = "Płaszcze", Drinks = "Kawa", Food = "FastFood" } },
               [2] = new Shop {Id = 2, Country = "Niemcy", City = "Berlin", Street = "Karlsstrasse", StreetNumber = "5345", Items = new Items {Id = 2, Clothes = "Kurtki", Drinks = "Kawa, Herbata"} },
               [3] = new Shop {Id = 3, Country = "Czechy", City = "Praga", Street = "Velka", StreetNumber = "342", Items = new Items {Id= 3, Clothes = "Buty" } }
            };
        }
    }
}
