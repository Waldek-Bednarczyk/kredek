namespace WaldemarBednarczykZadLab7.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WaldemarBednarczykZadLab7.Models.ShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WaldemarBednarczykZadLab7.Models.ShopContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Shops.AddOrUpdate(
                p => p.Id,
                new Models.Shop { Country = "Polska", City = "Wroc�aw", Street = "Grunwaldzka", StreetNumber = "32", Items = new Models.Items { Clothes = "P�aszcze", Drinks = "Kawa", Food = "FastFood" } },
                new Models.Shop { Country = "Niemcy", City = "Berlin", Street = "Karlsstrasse", StreetNumber = "5345", Items = new Models.Items { Clothes = "Kurtki", Drinks = "Kawa, Herbata"} },
                new Models.Shop { Country = "Czechy", City = "Praga", Street = "Velka", StreetNumber = "342", Items = new Models.Items { Clothes = "Buty" } }
                );
            

        }

    }
}
