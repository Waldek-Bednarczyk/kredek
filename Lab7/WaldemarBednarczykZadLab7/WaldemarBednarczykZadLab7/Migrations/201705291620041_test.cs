namespace WaldemarBednarczykZadLab7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "Clothes", c => c.String());
            AddColumn("dbo.Items", "Drinks", c => c.String());
            AddColumn("dbo.Items", "Food", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "Food");
            DropColumn("dbo.Items", "Drinks");
            DropColumn("dbo.Items", "Clothes");
        }
    }
}
