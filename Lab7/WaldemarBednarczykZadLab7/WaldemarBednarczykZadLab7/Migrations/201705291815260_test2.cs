namespace WaldemarBednarczykZadLab7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Shops");
            AlterColumn("dbo.Shops", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Shops", "Country", c => c.String(nullable: false));
            AlterColumn("dbo.Shops", "City", c => c.String(nullable: false));
            AlterColumn("dbo.Shops", "Street", c => c.String(nullable: false));
            AlterColumn("dbo.Shops", "StreetNumber", c => c.String(nullable: false));
            AddPrimaryKey("dbo.Shops", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Shops");
            AlterColumn("dbo.Shops", "StreetNumber", c => c.String());
            AlterColumn("dbo.Shops", "Street", c => c.String());
            AlterColumn("dbo.Shops", "City", c => c.String());
            AlterColumn("dbo.Shops", "Country", c => c.String());
            AlterColumn("dbo.Shops", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Shops", "Id");
        }
    }
}
