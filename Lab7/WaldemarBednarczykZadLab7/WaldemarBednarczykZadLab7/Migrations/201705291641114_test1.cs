namespace WaldemarBednarczykZadLab7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Shops");
            AlterColumn("dbo.Shops", "ID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Shops", "Id");
            DropColumn("dbo.Shops", "CreatedAt");
            DropColumn("dbo.Items", "CreatedAt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Shops", "CreatedAt", c => c.DateTime(nullable: false));
            DropPrimaryKey("dbo.Shops");
            AlterColumn("dbo.Shops", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Shops", "Id");
        }
    }
}
