﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZadLab3
{
    class Client
    {
        /// <summary>
        /// Wyświetlenie wszystkich klientów
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        public static void ShowAllClients(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(@"SELECT Name As 'Imię', Surname As 'Nazwisko',
            PhoneNumber As 'Numer Telefonu', Date As 'Data', City As 'Miasto', Street As 'Ulica', Number As 'Numer 'FROM Client", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
