﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZadLab3
{
    class CarDealer
    {
        /// <summary>
        /// Wyświetlenie wszystkich salonów
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        public static void ShowAllCarDealers(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(@"SELECT CarDealer.Province As 'Region', CarDealer.City AS 'Miasto',
                                                    CarDealer.Street As 'Ulica', CarDealer.Number As 'Numer', Country.Country As 'Kraj'
                                                FROM CarDealer JOIN Country ON Country.ID = CarDealer.CountryID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
