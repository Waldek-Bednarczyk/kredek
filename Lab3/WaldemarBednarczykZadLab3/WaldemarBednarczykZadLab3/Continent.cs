﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WaldemarBednarczykZadLab3
{
    class Continent
    {
        /// <summary>
        /// Wyświetlenie wszystkich kontynentów
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        public static void ShowAllContinents(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(@"SELECT Continent As 'Kontynent' FROM Continent", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
