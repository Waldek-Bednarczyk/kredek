﻿namespace WaldemarBednarczykZadLab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewCarDealer = new System.Windows.Forms.DataGridView();
            this.buttonContinent = new System.Windows.Forms.Button();
            this.buttonCountries = new System.Windows.Forms.Button();
            this.buttonCarDealer = new System.Windows.Forms.Button();
            this.buttonClient = new System.Windows.Forms.Button();
            this.buttonProduct = new System.Windows.Forms.Button();
            this.textBoxNewPrice = new System.Windows.Forms.TextBox();
            this.buttonAddProduct = new System.Windows.Forms.Button();
            this.textBoxNewModel = new System.Windows.Forms.TextBox();
            this.labelNewPrice = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelNewEngine = new System.Windows.Forms.Label();
            this.labelNewColor = new System.Windows.Forms.Label();
            this.textBoxNewVersion = new System.Windows.Forms.TextBox();
            this.textBoxNewEngine = new System.Windows.Forms.TextBox();
            this.textBoxNewColor = new System.Windows.Forms.TextBox();
            this.labelNewModel = new System.Windows.Forms.Label();
            this.textBoxSearchByModel = new System.Windows.Forms.TextBox();
            this.buttonSearchByModel = new System.Windows.Forms.Button();
            this.DeleteProduct = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCarDealer)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewCarDealer
            // 
            this.dataGridViewCarDealer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCarDealer.Location = new System.Drawing.Point(13, 14);
            this.dataGridViewCarDealer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewCarDealer.Name = "dataGridViewCarDealer";
            this.dataGridViewCarDealer.RowTemplate.Height = 24;
            this.dataGridViewCarDealer.Size = new System.Drawing.Size(474, 442);
            this.dataGridViewCarDealer.TabIndex = 0;
            // 
            // buttonContinent
            // 
            this.buttonContinent.Location = new System.Drawing.Point(801, 14);
            this.buttonContinent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonContinent.Name = "buttonContinent";
            this.buttonContinent.Size = new System.Drawing.Size(130, 36);
            this.buttonContinent.TabIndex = 1;
            this.buttonContinent.Text = "Kontynenty";
            this.buttonContinent.UseVisualStyleBackColor = true;
            this.buttonContinent.Click += new System.EventHandler(this.buttonContinent_Click);
            // 
            // buttonCountries
            // 
            this.buttonCountries.Location = new System.Drawing.Point(801, 60);
            this.buttonCountries.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonCountries.Name = "buttonCountries";
            this.buttonCountries.Size = new System.Drawing.Size(130, 36);
            this.buttonCountries.TabIndex = 2;
            this.buttonCountries.Text = "Kraje";
            this.buttonCountries.UseVisualStyleBackColor = true;
            this.buttonCountries.Click += new System.EventHandler(this.buttonCountries_Click);
            // 
            // buttonCarDealer
            // 
            this.buttonCarDealer.Location = new System.Drawing.Point(801, 106);
            this.buttonCarDealer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonCarDealer.Name = "buttonCarDealer";
            this.buttonCarDealer.Size = new System.Drawing.Size(130, 36);
            this.buttonCarDealer.TabIndex = 3;
            this.buttonCarDealer.Text = "Salony";
            this.buttonCarDealer.UseVisualStyleBackColor = true;
            this.buttonCarDealer.Click += new System.EventHandler(this.buttonCarDealer_Click);
            // 
            // buttonClient
            // 
            this.buttonClient.Location = new System.Drawing.Point(801, 152);
            this.buttonClient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonClient.Name = "buttonClient";
            this.buttonClient.Size = new System.Drawing.Size(130, 36);
            this.buttonClient.TabIndex = 5;
            this.buttonClient.Text = "Klient";
            this.buttonClient.UseVisualStyleBackColor = true;
            this.buttonClient.Click += new System.EventHandler(this.buttonClient_Click);
            // 
            // buttonProduct
            // 
            this.buttonProduct.Location = new System.Drawing.Point(801, 198);
            this.buttonProduct.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonProduct.Name = "buttonProduct";
            this.buttonProduct.Size = new System.Drawing.Size(130, 36);
            this.buttonProduct.TabIndex = 6;
            this.buttonProduct.Text = "Produkty";
            this.buttonProduct.UseVisualStyleBackColor = true;
            this.buttonProduct.Click += new System.EventHandler(this.buttonProduct_Click);
            // 
            // textBoxNewPrice
            // 
            this.textBoxNewPrice.Location = new System.Drawing.Point(1070, 35);
            this.textBoxNewPrice.Name = "textBoxNewPrice";
            this.textBoxNewPrice.Size = new System.Drawing.Size(100, 30);
            this.textBoxNewPrice.TabIndex = 7;
            // 
            // buttonAddProduct
            // 
            this.buttonAddProduct.Location = new System.Drawing.Point(1067, 223);
            this.buttonAddProduct.Name = "buttonAddProduct";
            this.buttonAddProduct.Size = new System.Drawing.Size(103, 67);
            this.buttonAddProduct.TabIndex = 8;
            this.buttonAddProduct.Text = "Dodaj Produkt";
            this.buttonAddProduct.UseVisualStyleBackColor = true;
            this.buttonAddProduct.Click += new System.EventHandler(this.buttonAddProduct_Click);
            // 
            // textBoxNewModel
            // 
            this.textBoxNewModel.Location = new System.Drawing.Point(1069, 71);
            this.textBoxNewModel.Name = "textBoxNewModel";
            this.textBoxNewModel.Size = new System.Drawing.Size(100, 30);
            this.textBoxNewModel.TabIndex = 9;
            // 
            // labelNewPrice
            // 
            this.labelNewPrice.AutoSize = true;
            this.labelNewPrice.Location = new System.Drawing.Point(1003, 35);
            this.labelNewPrice.Name = "labelNewPrice";
            this.labelNewPrice.Size = new System.Drawing.Size(60, 25);
            this.labelNewPrice.TabIndex = 10;
            this.labelNewPrice.Text = "Cena";
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(990, 107);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(74, 25);
            this.labelVersion.TabIndex = 12;
            this.labelVersion.Text = "Wersja";
            // 
            // labelNewEngine
            // 
            this.labelNewEngine.AutoSize = true;
            this.labelNewEngine.Location = new System.Drawing.Point(1004, 143);
            this.labelNewEngine.Name = "labelNewEngine";
            this.labelNewEngine.Size = new System.Drawing.Size(59, 25);
            this.labelNewEngine.TabIndex = 13;
            this.labelNewEngine.Text = "Silnik";
            // 
            // labelNewColor
            // 
            this.labelNewColor.AutoSize = true;
            this.labelNewColor.Location = new System.Drawing.Point(1005, 179);
            this.labelNewColor.Name = "labelNewColor";
            this.labelNewColor.Size = new System.Drawing.Size(58, 25);
            this.labelNewColor.TabIndex = 14;
            this.labelNewColor.Text = "Kolor";
            // 
            // textBoxNewVersion
            // 
            this.textBoxNewVersion.Location = new System.Drawing.Point(1070, 107);
            this.textBoxNewVersion.Name = "textBoxNewVersion";
            this.textBoxNewVersion.Size = new System.Drawing.Size(100, 30);
            this.textBoxNewVersion.TabIndex = 15;
            // 
            // textBoxNewEngine
            // 
            this.textBoxNewEngine.Location = new System.Drawing.Point(1069, 143);
            this.textBoxNewEngine.Name = "textBoxNewEngine";
            this.textBoxNewEngine.Size = new System.Drawing.Size(100, 30);
            this.textBoxNewEngine.TabIndex = 16;
            // 
            // textBoxNewColor
            // 
            this.textBoxNewColor.Location = new System.Drawing.Point(1069, 179);
            this.textBoxNewColor.Name = "textBoxNewColor";
            this.textBoxNewColor.Size = new System.Drawing.Size(100, 30);
            this.textBoxNewColor.TabIndex = 17;
            // 
            // labelNewModel
            // 
            this.labelNewModel.AutoSize = true;
            this.labelNewModel.Location = new System.Drawing.Point(997, 71);
            this.labelNewModel.Name = "labelNewModel";
            this.labelNewModel.Size = new System.Drawing.Size(66, 25);
            this.labelNewModel.TabIndex = 18;
            this.labelNewModel.Text = "Model";
            // 
            // textBoxSearchByModel
            // 
            this.textBoxSearchByModel.Location = new System.Drawing.Point(889, 371);
            this.textBoxSearchByModel.Name = "textBoxSearchByModel";
            this.textBoxSearchByModel.Size = new System.Drawing.Size(100, 30);
            this.textBoxSearchByModel.TabIndex = 19;
            this.textBoxSearchByModel.TextChanged += new System.EventHandler(this.textBoxSearchByModel_TextChanged);
            // 
            // buttonSearchByModel
            // 
            this.buttonSearchByModel.Location = new System.Drawing.Point(995, 357);
            this.buttonSearchByModel.Name = "buttonSearchByModel";
            this.buttonSearchByModel.Size = new System.Drawing.Size(109, 59);
            this.buttonSearchByModel.TabIndex = 20;
            this.buttonSearchByModel.Text = "Wyszukaj Modelu";
            this.buttonSearchByModel.UseVisualStyleBackColor = true;
            // 
            // DeleteProduct
            // 
            this.DeleteProduct.Location = new System.Drawing.Point(601, 305);
            this.DeleteProduct.Name = "DeleteProduct";
            this.DeleteProduct.Size = new System.Drawing.Size(165, 82);
            this.DeleteProduct.TabIndex = 21;
            this.DeleteProduct.Text = "Usuń";
            this.DeleteProduct.UseVisualStyleBackColor = true;
            this.DeleteProduct.Click += new System.EventHandler(this.DeleteProduct_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 565);
            this.Controls.Add(this.DeleteProduct);
            this.Controls.Add(this.buttonSearchByModel);
            this.Controls.Add(this.textBoxSearchByModel);
            this.Controls.Add(this.labelNewModel);
            this.Controls.Add(this.textBoxNewColor);
            this.Controls.Add(this.textBoxNewEngine);
            this.Controls.Add(this.textBoxNewVersion);
            this.Controls.Add(this.labelNewColor);
            this.Controls.Add(this.labelNewEngine);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelNewPrice);
            this.Controls.Add(this.textBoxNewModel);
            this.Controls.Add(this.buttonAddProduct);
            this.Controls.Add(this.textBoxNewPrice);
            this.Controls.Add(this.buttonProduct);
            this.Controls.Add(this.buttonClient);
            this.Controls.Add(this.buttonCarDealer);
            this.Controls.Add(this.buttonCountries);
            this.Controls.Add(this.buttonContinent);
            this.Controls.Add(this.dataGridViewCarDealer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCarDealer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewCarDealer;
        private System.Windows.Forms.Button buttonContinent;
        private System.Windows.Forms.Button buttonCountries;
        private System.Windows.Forms.Button buttonCarDealer;
        private System.Windows.Forms.Button buttonClient;
        private System.Windows.Forms.Button buttonProduct;
        private System.Windows.Forms.TextBox textBoxNewPrice;
        private System.Windows.Forms.Button buttonAddProduct;
        private System.Windows.Forms.TextBox textBoxNewModel;
        private System.Windows.Forms.Label labelNewPrice;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelNewEngine;
        private System.Windows.Forms.Label labelNewColor;
        private System.Windows.Forms.TextBox textBoxNewVersion;
        private System.Windows.Forms.TextBox textBoxNewEngine;
        private System.Windows.Forms.TextBox textBoxNewColor;
        private System.Windows.Forms.Label labelNewModel;
        private System.Windows.Forms.TextBox textBoxSearchByModel;
        private System.Windows.Forms.Button buttonSearchByModel;
        private System.Windows.Forms.Button DeleteProduct;
    }
}

