﻿using System.Data.SqlClient;
using System.Windows.Forms;

namespace WaldemarBednarczykZadLab3
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        public Form1()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source=DESKTOP-BTJSU9U; database=DealerNetwork;Trusted_Connection=yes");
        }

        private void buttonContinent_Click(object sender, System.EventArgs e)
        {
            Continent.ShowAllContinents(sqlConnection, dataGridViewCarDealer);
        }

        private void buttonCountries_Click(object sender, System.EventArgs e)
        {
            Country.ShowAllCountries(sqlConnection, dataGridViewCarDealer);
        }

        private void buttonCarDealer_Click(object sender, System.EventArgs e)
        {
            CarDealer.ShowAllCarDealers(sqlConnection, dataGridViewCarDealer);
        }

        private void buttonClient_Click(object sender, System.EventArgs e)
        {
            Client.ShowAllClients(sqlConnection, dataGridViewCarDealer);
        }

        private void buttonProduct_Click(object sender, System.EventArgs e)
        {
            Product.ShowAllProducts(sqlConnection, dataGridViewCarDealer);
        }

        private void buttonAddProduct_Click(object sender, System.EventArgs e)
        {
            Product.AddProduct(sqlConnection, dataGridViewCarDealer, textBoxNewPrice.Text, textBoxNewModel.Text, textBoxNewVersion.Text,
                textBoxNewEngine.Text, textBoxNewColor.Text);
        }

        private void textBoxSearchByModel_TextChanged(object sender, System.EventArgs e)
        {
            Product.FindByModel(sqlConnection, dataGridViewCarDealer, textBoxSearchByModel.Text);
        }

        private void DeleteProduct_Click(object sender, System.EventArgs e)
        {
            Product.DeleteProduct(sqlConnection, dataGridViewCarDealer, textBoxNewPrice.Text, textBoxNewModel.Text, textBoxNewVersion.Text,
                textBoxNewEngine.Text, textBoxNewColor.Text);
        }
    }
}
