﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaldemarBednarczykZadLab3
{
    class Product
    {
        static SqlCommand sqlCommand;
        /// <summary>
        /// Wyświetlenie wszystkich produktów
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        public static void ShowAllProducts(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(@"SELECT Price As 'Cena', Model, Version As 'Wersja', Engine As 'Silnik',
            Color As 'Kolor' FROM Product", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Dodawanie produktu
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        /// <param name="price"></param>
        /// <param name="model"></param>
        /// <param name="version"></param>
        /// <param name="engine"></param>
        /// <param name="color"></param>
        public static void AddProduct(SqlConnection sqlConnection, DataGridView dataGridView, string price, string model, string version, string engine, string color)
        {
            try
            {
                sqlConnection.Open();
                string command = $"INSERT INTO Product (Price,Model,Version,Engine,Color) values ('{price}','{model}','{version}','{engine}','{color}')";
                sqlCommand = new SqlCommand(command, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Dodano");
                ShowAllProducts(sqlConnection, dataGridView);
                sqlConnection.Close();
            }
            catch
            {
                MessageBox.Show("Błąd");
            }
        }

        public static void DeleteProduct(SqlConnection sqlConnection, DataGridView dataGridView, string price, string model, string version, string engine, string color)
        {
            try
            {
                sqlConnection.Open();
                string command = "DELETE FROM Product (Price,Model,Version,Engine,Color) values ('{price}','{model}','{version}','{engine}','{color}')";
                sqlCommand = new SqlCommand(command, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Usunięto");
                ShowAllProducts(sqlConnection, dataGridView);
                sqlConnection.Close();
            }
            catch
            {
                MessageBox.Show("Niestety nie udalo sie");
            }
        }
        /// <summary>
        /// Wyszukiwanie po modelu
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        /// <param name="name"></param>
        public static void FindByModel(SqlConnection sqlConnection, DataGridView dataGridView, string name)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($"SELECT* FROM Product WHERE Model LIKE '%{name}%'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
