﻿namespace WaldemarBednarczykLab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewZoo = new System.Windows.Forms.DataGridView();
            this.buttonShowAllAnimals = new System.Windows.Forms.Button();
            this.buttonShowSloths = new System.Windows.Forms.Button();
            this.textBoxSpecies = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.buttonAddAnimal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZoo)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewZoo
            // 
            this.dataGridViewZoo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewZoo.Location = new System.Drawing.Point(30, 62);
            this.dataGridViewZoo.Name = "dataGridViewZoo";
            this.dataGridViewZoo.RowTemplate.Height = 24;
            this.dataGridViewZoo.Size = new System.Drawing.Size(550, 314);
            this.dataGridViewZoo.TabIndex = 0;
            // 
            // buttonShowAllAnimals
            // 
            this.buttonShowAllAnimals.Location = new System.Drawing.Point(82, 414);
            this.buttonShowAllAnimals.Name = "buttonShowAllAnimals";
            this.buttonShowAllAnimals.Size = new System.Drawing.Size(125, 70);
            this.buttonShowAllAnimals.TabIndex = 1;
            this.buttonShowAllAnimals.Text = "Pokaż Wszystkie";
            this.buttonShowAllAnimals.UseVisualStyleBackColor = true;
            this.buttonShowAllAnimals.Click += new System.EventHandler(this.buttonShowAllAnimals_Click);
            // 
            // buttonShowSloths
            // 
            this.buttonShowSloths.Location = new System.Drawing.Point(284, 414);
            this.buttonShowSloths.Name = "buttonShowSloths";
            this.buttonShowSloths.Size = new System.Drawing.Size(105, 75);
            this.buttonShowSloths.TabIndex = 2;
            this.buttonShowSloths.Text = " Pokaż Leniwce";
            this.buttonShowSloths.UseVisualStyleBackColor = true;
            this.buttonShowSloths.Click += new System.EventHandler(this.buttonShowSloths_Click);
            // 
            // textBoxSpecies
            // 
            this.textBoxSpecies.Location = new System.Drawing.Point(698, 80);
            this.textBoxSpecies.Name = "textBoxSpecies";
            this.textBoxSpecies.Size = new System.Drawing.Size(100, 22);
            this.textBoxSpecies.TabIndex = 3;
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(698, 130);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 22);
            this.textBoxAmount.TabIndex = 4;
            // 
            // buttonAddAnimal
            // 
            this.buttonAddAnimal.Location = new System.Drawing.Point(680, 158);
            this.buttonAddAnimal.Name = "buttonAddAnimal";
            this.buttonAddAnimal.Size = new System.Drawing.Size(138, 66);
            this.buttonAddAnimal.TabIndex = 5;
            this.buttonAddAnimal.Text = "Dodaj Zwierzę";
            this.buttonAddAnimal.UseVisualStyleBackColor = true;
            this.buttonAddAnimal.Click += new System.EventHandler(this.buttonAddAnimal_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 517);
            this.Controls.Add(this.buttonAddAnimal);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.textBoxSpecies);
            this.Controls.Add(this.buttonShowSloths);
            this.Controls.Add(this.buttonShowAllAnimals);
            this.Controls.Add(this.dataGridViewZoo);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZoo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewZoo;
        private System.Windows.Forms.Button buttonShowAllAnimals;
        private System.Windows.Forms.Button buttonShowSloths;
        private System.Windows.Forms.TextBox textBoxSpecies;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.Button buttonAddAnimal;
    }
}

