﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WaldemarBednarczykLab3
{
    class Animals
    {
        public static void ShowAllAnimals(SqlConnection sqlConnection, DataGridView dataGridView)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT Species as Gatunek, Amount as Ilość FROM Animals ",sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void AddAnimal(SqlConnection sqlConnection, DataGridView dataGridView, string species, string amount)
        {
            sqlConnection.Open();
            string command = $"INSERT INTO Animals (Species,Amount) values ('{species}','{amount}')";
            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);
            sqlCommand.ExecuteNonQuery();
            MessageBox.Show("Udało się");
            ShowAllAnimals(sqlConnection, dataGridView);
            sqlConnection.Close();
        }
    }
}
