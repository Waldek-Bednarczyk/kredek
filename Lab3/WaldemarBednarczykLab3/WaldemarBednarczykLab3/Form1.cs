﻿using System.Data.SqlClient;
using System.Windows.Forms;

namespace WaldemarBednarczykLab3
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        SqlDataAdapter sqlDataAdapter;

        public Form1()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = DESKTOP-BTJSU9U; database = ZOO; Trusted_Connection=yes");
        }

        private void buttonShowAllAnimals_Click(object sender, System.EventArgs e)
        {
            Animals.ShowAllAnimals(sqlConnection, dataGridViewZoo);
        }

        private void buttonShowSloths_Click(object sender, System.EventArgs e)
        {
            Sloths.ShowAllAnimals(sqlConnection, dataGridViewZoo);
        }

        private void buttonAddAnimal_Click(object sender, System.EventArgs e)
        {
            Animals.AddAnimal(sqlConnection, dataGridViewZoo, textBoxSpecies.Text, textBoxAmount.Text);
        }
    }
}
