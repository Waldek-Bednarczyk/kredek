﻿using Xunit;
using Maths;

namespace MathTest
{
    public class MathTest
    {
        private MathOperations _sut;
        public MathTest()
        {
            _sut = new MathOperations();
        }

        [Fact]
        public void Factorial_of_5_is_120()
        {
            //Arrage
            int x = 5;
            int expectedResult = 120;
            //Act
            int actualResult = _sut.Factorial(x);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Power_of_5_to_4_is_625()
        {
            //Arrage
            double x = 5;
            double y = 4;
            double expectedResult = 625;
            //Act
            double actualResult = _sut.Powering(x, y);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Exp_of_e_to_5_is_approximately_20()
        {
            //Arrage
            double x = 5;
            double expectedResult = 148;
            //Act
            double actualResult = _sut.Exp(x);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void If_4_is_even_number()
        {
            //Arrage
            double x = 4;
            bool expectedResult = true;
            //Act
            bool actualResult = _sut.Even(x);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
