﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalData
{
    public class Person
    {
        public string GetGender(string name)
        {
            if (name.EndsWith("a")) return "kobieta";
            return "mężczyzna";
        }

        public string AgeCategory(int age, string gender)
        {
            List<string> stageOfLife = new List<string>(3);
            stageOfLife.Add("młodość");
            stageOfLife.Add("wiek średni");
            stageOfLife.Add("starość");
            if (age < 0 || age>124) throw new Exception("Zła wartość zmiennej przechowującej wiek");
            if (gender == "kobieta")
            {
                if (age < 26) return stageOfLife[0];
                if (age >= 26 && age <= 68) return stageOfLife[1];
                if (age > 68) return stageOfLife[2];
            }
            if (gender == "mężczyzna")
            {
                if (age < 24) return stageOfLife[0];
                if (age >= 24 && age <= 64) return stageOfLife[1];
                if (age > 64) return stageOfLife[2];
            }
            throw new Exception("Nieznana płeć");
        }

        public string HeightCategory(double height, string gender)
        {
            List<string> heightCategory = new List<string>(3);
            heightCategory.Add("niska/niski");
            heightCategory.Add("średnia/średni");
            heightCategory.Add("wysoka/wysoki");

            if (height < 0 || height > 260) throw new Exception("Zła wartość zmiennej przechowującej wzrost");
            if (gender == "kobieta")
            {
                if (height < 160) return heightCategory[0];
                if (height >= 161 && height <= 170) return heightCategory[1];
                if (height > 170 ) return heightCategory[2];
            }
            if (gender == "mężczyzna")
            {
                if (height < 168) return heightCategory[0];
                if (height >= 168 && height <= 184) return heightCategory[1];
                if (height > 184) return heightCategory[2];
            }
            throw new Exception("Nieznana płeć");
        }

        public string CheckIDGender (string id)
        {
            int number = Convert.ToInt16(id.Substring(9, 1));
            if(number %10 == 0 ) return "Kobieta";
            return "Mężczyzna";
        }
    }
}
