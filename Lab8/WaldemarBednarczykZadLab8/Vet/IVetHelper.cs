﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vet
{
    public interface IVetHelper
    {
        double GetPrice(string services);
        bool AddAnimal(string animal);
        bool AddDoctor(int id,string name, string surname, int age, string specification);
    }
}
