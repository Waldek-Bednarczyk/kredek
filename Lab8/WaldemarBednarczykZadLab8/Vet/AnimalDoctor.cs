﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vet
{
    public class AnimalDoctor : IVetHelper
    {
        public IList<string> animals = new List<string>();
        public IList<Doctor> doctors = new List<Doctor>();
        public double GetPrice(string services)
        {
            if (services == "badanie") return 30;
            if (services == "zastrzyk") return 80;
            if (services == "operacja") return 400;
            if (services == "uśpienie") return 200;
            if (services == "przewóz") return 97;
            return 0;
        }

        public bool AddAnimal(string animal)
        {
            if (animals.Contains(animal)) return false;
            animals.Add(animal);
            return true;
        }

        public bool AddDoctor(int id, string name, string surname, int age, string specification)
        {
            Doctor doctor = new Doctor(id, name, surname, age, specification);
            if (doctors.Contains(doctor)) return false;
            doctors.Add(doctor);
            return true;
        }

        public int MatchDoctorToAnimal(string animal)
        {
            foreach(var doctor in doctors)
            {
                if (doctor.specification == animal) return doctor.id;
            }
            return -1;
        }

    }
}
