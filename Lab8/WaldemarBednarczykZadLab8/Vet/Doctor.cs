﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vet
{
    public class Doctor
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int age { get; set; }
        public string specification { get; set; }
        public Doctor(int idTemp, string nameTemp, string surnameTemp, int ageTemp, string specificationTemp)
        {
            id = idTemp;
            name = nameTemp;
            surname = surnameTemp;
            age = ageTemp;
            specification = specificationTemp;
        }
    }
}
