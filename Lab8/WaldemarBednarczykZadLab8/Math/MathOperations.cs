﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
  
    public class MathOperations
    {
        public int Factorial(int x)
        {
            if (x <= 1) return 1;
            return x * Factorial(x - 1);
        }

        public double Powering(double x, double y)
        {
            return Math.Pow(x, y);
        }

        public double Exp(double x)
        {
            return Math.Round(Math.Exp(x));
        }

        public bool Even(double x)
        {
            if ( (x%2) == 0) return true;
            return false;
        }
    }
}
