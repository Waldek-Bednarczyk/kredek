﻿using System;
using PersonalData;
using Xunit;

namespace PersonalDataTest
{
   
    public class PersonalDataTest
    {
        private Person _sut;
        public PersonalDataTest()
        {
            _sut = new Person();
        }

        [Fact]
        public void Gender_of_Anna_is_women()
        {
            //Arrage
            string name = "Anna";
            string expectedResult = "kobieta";
            //Act
            string actualResult = _sut.GetGender(name);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Age_category_of_women_aged_22_is_young()
        {
            //Arrage
            int age = 22;
            string gender = "kobieta";
            string expectedResult = "młodość";
            //Act
            string actualResult = _sut.AgeCategory(age, gender);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Height_category_of_men_whose_height_is_203_is_tall()
        {
            //Arrage
            int height = 203;
            string gender = "mężczyzna";
            string expectedResult = "wysoka/wysoki";
            //Act
            string actualResult = _sut.HeightCategory(height,gender);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Gender_from_id_number_which_10_letter_is_3_is_man()
        {
            //Arrage
            string id = "63728104939";
            string expectedResult = "Mężczyzna";
            //Act
            string actualResult = _sut.CheckIDGender(id);
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
