﻿using System;
using Xunit;
using Vet;
using FakeItEasy;
using System.Collections.Generic;

namespace VetTest
{
    
    public class AnimalDoctorTest
    {
        private AnimalDoctor _sut;

        public AnimalDoctorTest()
        {
            var fakeIVetHelper = A.Fake<IVetHelper>();
            A.CallTo(() => fakeIVetHelper.Equals(3));
            _sut = new AnimalDoctor();
        }
        [Fact]
        public void Price_of_badanie_is_30()
        {
            string name = "badanie";
            double expectedResult = 30;
            double actualResult = _sut.GetPrice(name);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Adding_again_Kot_item_to_list_results_in_false()
        {
            _sut.AddAnimal("Kot");
            string name = "Kot";
            bool expectedResult = false;
            bool actualResult = _sut.AddAnimal(name);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Adding_new_doctor_results_in_true()
        {
            int id = 1;
            string name = "Adam";
            string surname = "Kowalski";
            int age = 34;
            string specification = "Pies";
            bool expectedResult = true;
            bool actualResult = _sut.AddDoctor(id,name, surname, age, specification);
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Matching_doctor_whose_specification_is_Kot_doesnt_match_animal_which_is_Psy()
        {
            int id = 1;
            string name = "Adam";
            string surname = "Kowalski";
            int age = 34;
            string specification = "Pies";
            int expectedResult = -1;
            _sut.AddDoctor(id, name, surname, age, specification);
            int actualResult = _sut.MatchDoctorToAnimal("Kot");
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
