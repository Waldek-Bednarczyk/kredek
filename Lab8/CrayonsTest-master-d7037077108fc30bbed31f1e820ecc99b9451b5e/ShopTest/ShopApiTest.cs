﻿using System;
using Shop.Controllers;
using Xunit;
using Shop.Repository.Interfaces;
using FakeItEasy;
using System.Collections.Generic;
using Shop.Model;

namespace ShopTest
{
    
    public class ShopApiTest
    {
        ShopController _sut;
        public ShopApiTest()
        {
            var fakeRepository = A.Fake<IReadItemRepository>();
            A.CallTo(() => fakeRepository.GetAll()).Returns(GetFakeItems());
            A.CallTo(() => fakeRepository.GetById(1)).Returns(new Item() { Id = 1, Name = "Czapka", Price = 10 });
            A.CallTo(() => fakeRepository.GetUsersBasket(1)).Returns(GetFakeItems());
            _sut = new ShopController(fakeRepository);
        }
        [Fact]
        public void GetApp_Returns_Everything()
        {
            //Arrange
            var expectedResult = GetFakeItems();
            //Act
            var actualResult = _sut.GetAll();

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
            Assert.Equal(expectedResult[0].Id, actualResult[0].Id);
            Assert.Equal(expectedResult[1].Name, actualResult[1].Name);
            Assert.Equal(expectedResult[2].Price, actualResult[2].Price);
        }

        [Fact]
        public void GetById_returns_item_with_that_id()
        {
            //Arrange
            int id = 1;
            var expectedResult = new Item() { Id = 1, Name = "Czapka", Price = 10 };

            //Act
            var actualResult = _sut.GetItem(id);

            //Assert
            Assert.Equal(expectedResult.Id, actualResult.Id);
            Assert.Equal(expectedResult.Name, actualResult.Name);
            Assert.Equal(expectedResult.Price, actualResult.Price);
        }

        [Fact]
        public void GetBasket_returns_user_basket()
        {
            //Arrange
            int id = 1;
            var expectedResult = GetFakeItems();
            //Act
            var actualResult = _sut.GetUserBasket(id);

            //Assert
            Assert.Equal(expectedResult.Count, actualResult.Count);
            Assert.Equal(expectedResult[0].Id, actualResult[0].Id);
            Assert.Equal(expectedResult[1].Name, actualResult[1].Name);
            Assert.Equal(expectedResult[2].Price, actualResult[2].Price);
        }

        private IList<Item> GetFakeItems()
        {
            var list = new List<Item>()
            {
                new Item(){ Id=1, Name="Czapka", Price=10},
                new Item(){ Id=2, Name="Spodnie", Price=20},
                new Item(){ Id=3, Name="Buty", Price=30}
            };
            return list;
        }
    }
}
