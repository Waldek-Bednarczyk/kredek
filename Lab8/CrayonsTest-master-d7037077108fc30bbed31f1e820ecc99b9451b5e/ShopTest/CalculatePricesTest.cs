﻿using Shop.Services;
using System;
using Xunit;
using FakeItEasy;
using Shop.Services.Iterfaces;
using Shop.Model;
using System.Collections.Generic;

namespace ShopTest
{
    public class CalculatePricesTest
    {
        private CalculatePrices _sut;
        public CalculatePricesTest()
        {
            var fakeDiscountHelper = A.Fake<IDiscountHelper>();
            A.CallTo(() => fakeDiscountHelper.Apply(25)).Returns(25);
            _sut = new CalculatePrices(fakeDiscountHelper);
        }
        [Fact]
        public void CalculatePrices_adds_prices_of_every_item_in_list()
        {
            //Arrange
            IList<Item> listOfItems = new List<Item>()
            {
                new Item(){Price = 10 },
                new Item(){Price = 10 },
                new Item(){Price = 5 }
            };
            decimal expectedResult = 25;
            //Act
            decimal actualResult = _sut.CalculatePrice(listOfItems);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
