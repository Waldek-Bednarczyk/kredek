﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using Xunit;
using Xunit.Abstractions;

namespace Documentation
{
    public class ApiTest
    {
        private readonly ITestOutputHelper _output;
        public ApiTest(ITestOutputHelper output)
        {
            _output = output;
        }
        private string Execute(string actionType, string query)
        {
            string url= "http://localhost:5000/" + actionType + "/" + query;
            return new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
        }

        private dynamic Deserialize(string value)
        {
            return JsonConvert.DeserializeObject<dynamic>(value);
        }
        [Fact]
        public void Api_is_working()
        {
            //Act
            var result = Execute("", "");

            _output.WriteLine(result);
            //Assert
            Assert.NotEmpty(result);
        }

        [Fact]
        public void By_giving_name_as_actiontype_we_can_find_books_by_name()
        {
            string title_expected = "Harry Potter";
            string author_expected = "J.K.Rowling";
            int rate_expected = 8;

            string result = Execute("name", "Harry Potter");
            dynamic json = Deserialize(result)["books"];
            string title_result = json[0]["title"];
            string author_result = json[0]["author"];
            int rate_result = json[0]["rate"];

            _output.WriteLine(result);
            Assert.Equal(title_expected, title_result);
            Assert.Equal(author_expected, author_result);
            Assert.Equal(rate_expected, rate_result);

        }

        [Fact]
        public void By_giving_rate_as_actiontype_we_can_find_books_by_rate()
        {
            string first_title_expected = "Eragon";
            string second_title_expected = "Lord of the Rings";

            string result = Execute("rated", "9");
            dynamic json = Deserialize(result)["books"];
            string first_title_result = json[0]["title"];
            string second_title_result = json[1]["title"];

            _output.WriteLine(result);
            Assert.Equal(first_title_expected, first_title_result);
            Assert.Equal(second_title_expected, second_title_result);

        }

        [Fact]
        public void By_giving_authors_name_as_actiontype_we_can_find_books_by_authors_name()
        {
            string title_expected = "Harry Potter";
            string author_expected = "J.K.Rowling";
            int rate_expected = 8;

            string result = Execute("author", "J.K.Rowling");
            dynamic json = Deserialize(result)["books"];
            string title_result = json[0]["title"];
            string author_result = json[0]["author"];
            int rate_result = json[0]["rate"];

            _output.WriteLine(result);
            Assert.Equal(title_expected, title_result);
            Assert.Equal(author_expected, author_result);
            Assert.Equal(rate_expected, rate_result);

        }
    }
}
