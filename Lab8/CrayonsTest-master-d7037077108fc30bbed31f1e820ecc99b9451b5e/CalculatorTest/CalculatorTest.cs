﻿using Xunit;
using Calculator;
using System;

namespace CalculatorTest
{
    public class CalculatorTest
    {
        private CrayonsCalculator _sut;
        public CalculatorTest()
        {
            _sut = new CrayonsCalculator();
        }

        [Fact]
        public void Adding_2_and_2_results_as_4()
        {
            //Arrage
            double x = 2;
            double y = 2;
            double expectedResult = 4;
            //Act
            double actualResult = _sut.Add(x, y);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Adding_2_and_3_results_as_5()
        {
            //Arrage
            double x = 2;
            double y = 3;
            double expectedResult = 5;
            //Act
            double actualResult = _sut.Add(x, y);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Substract_3_and_2_results_as_1()
        {
            //Arrage
            double x = 3;
            double y = 2;
            double expectedResult = 1;
            //Act
            double actualResult = _sut.Substract(x, y);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Multiplying_2_and_3_results_as_6()
        {
            //Arrage
            double x = 3;
            double y = 2;
            double expectedResult = 6;
            //Act
            double actualResult = _sut.Multiply(x, y);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Dividing_by_0_throws_exception()
        {
            //Arrange
            double x = 1;
            double y = 0;
            //Act


            //Assert
            Assert.Throws<Exception>(() => _sut.Divide(x, y));
        }

        [Fact]
        public void Dividing_4_by_2_results_as_2()
        {
            //Arrange
            double x = 4;
            double y = 2;
            double expectedResult = 2;

            //Act
            double actualRestult = _sut.Divide(x, y);
            //Assert
            Assert.Equal(expectedResult, actualRestult);
        }
    }
}
