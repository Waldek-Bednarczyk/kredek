﻿using Shop.Repository.Interfaces;
using Shop.Services.Iterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Model;

namespace Shop.Services
{
    public class CalculatePrices : ICalculatePrices
    {
        IDiscountHelper _discountHelper;
        public CalculatePrices(IDiscountHelper discountHelper)
        {
            _discountHelper = discountHelper;
        }

        public decimal CalculatePrice(IList<Item> items)
        {
            decimal result = 0;
            foreach(var item in items)
            {
                result += item.Price;
            }
            result = _discountHelper.Apply(result);
            return result;
        }
    }
}
