﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Iterfaces
{
    public interface ICalculatePrices
    {
        decimal CalculatePrice(IList<Item> items);
    }
}
