﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Iterfaces
{
    public interface IDiscountHelper
    {
        decimal Apply(decimal price);
    }
}
