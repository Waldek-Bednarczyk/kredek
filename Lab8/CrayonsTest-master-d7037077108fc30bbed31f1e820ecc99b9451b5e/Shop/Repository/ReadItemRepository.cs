﻿using Shop.Model;
using Shop.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Repository
{
    public class ReadItemRepository:IReadItemRepository
    {
        IList<Item> _items;
        public ReadItemRepository()
        {
            _items = new List<Item>();
        }

        public IList<Item> GetAll()
        {
            return _items;
        }

        public Item GetById(int id)
        {
            return new Item();
        }

        public IList<Item> GetUsersBasket(int userId)
        {
            throw new NotImplementedException();
        }
    }
}
