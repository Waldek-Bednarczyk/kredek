﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Repository.Interfaces
{
    public interface IReadItemRepository
    {
        IList<Item> GetUsersBasket(int userId);
        Item GetById(int id);
        IList<Item> GetAll();
    }
}
