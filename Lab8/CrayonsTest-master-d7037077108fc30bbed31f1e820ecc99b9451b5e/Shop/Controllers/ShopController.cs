﻿using Shop.Model;
using Shop.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Shop.Controllers
{
    [Route("/api")]
    public class ShopController:ApiController
    {
        private IReadItemRepository _itemRepository;
        public ShopController(IReadItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        [Route("/")]
        public IList<Item> GetAll()
        {
            return _itemRepository.GetAll();
        }

        [Route("/item/{id:int}")]
        public Item GetItem(int id)
        {
            return _itemRepository.GetById(id);
        }

        [Route("/basket/{id:int}")]
        public IList<Item> GetUserBasket(int id)
        {
            return _itemRepository.GetUsersBasket(id);
        }

    }
}
