﻿using System;

namespace Calculator
{
    public class CrayonsCalculator
    {
        public double Add(double x, double y)
        {
            return x + y;
        }

        public double Substract(double x, double y)
        {
            return x - y;
        }
        
        public double Multiply(double x, double y)
        {
            return x * y;
        }

        public double Divide(double x, double y)
        {
            if (y == 0) throw new Exception("Nie można dzielić przez zero");
            return x / y;
        }
    }
}
