﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaldemarBednarczykZad
{
    class Child : Base
    {
        private string _name;
        private int _age;

        public int Name { get; set; }
        public int Age { get; set; }

        public void SetName(string name)
        {
            _name = name ;
        }
        public string GetName()
        {
            return _name;
        }

        public int GetAge()
        {
            return _age;
        }

        enum Count
        {
            old, young
        };
    }
}
