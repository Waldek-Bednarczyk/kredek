﻿using System;

namespace WaldemarBednarczykZadLab2
{
    class Vehicle : Base, IMovable
    {
        public string GetWelcome()
        {
            return welcome;
        }
        public string Print(string name)
        {
            return ("Oto " + name);
        }
    }
}
