﻿using System;
using System.Collections.Generic;

namespace WaldemarBednarczykZadLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle welcome = new Vehicle();
            System.Console.WriteLine(welcome.GetWelcome());

            List<Car> cars = new List<Car>
            {
                new Car() {Name = "Auto 1" },
                new Car() {Name = "Auto 2" },
                new Car() {Name = "Auto 3" }
            };

            List<Bicycle> bicycles = new List<Bicycle>
            {
                new Bicycle() {Name = "Rower 1" },
                new Bicycle() {Name = "Rower 2" },
                new Bicycle() {Name = "Rower 3" }
            };

            foreach (Car c in cars)
            System.Console.WriteLine(c.SayName());

            foreach (Bicycle b in bicycles)
             System.Console.WriteLine(b.SayName());

            Console.Read();
        }
    }
}
