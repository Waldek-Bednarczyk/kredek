﻿using System;

namespace WaldemarBednarczykZadLab2
{
    class Bicycle : Vehicle
    {
        public string Name { get; set; }
        public string SayName()
        {
            return Print(Name);
        }
    }
}
