﻿using System;

namespace WaldemarBednarczykZadLab2
{
    class Car : Vehicle
    {
        public string Name { get; set; }
        public string SayName()
        {
            return Print(Name);
        }
    }
}
